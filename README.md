# esp8266-doit-grove-kit
This Project provides a starting point for the ESP8266-based Grove board.
**Please consider this repository as 'work in progress'**.

## Getting the board
The most official webpage seems to be [here][1].
The boards are available at [AliExpress][2] for about 10$.

## Preinstalled firmware
This [Github project][3] is the official repository of SmartArduino

## Initializing the project with platformio
```
platformio init --board esp12e --ide clion
```

## Pinout
Doit Grove Board | ESP12F
:--------------: | :----:
![Alt text](doc/doit_board_layout.jpg) | ![Alt text](doc/esp-12-pinout.jpg)

Pin on Doit Board | Socket | Pin on ESP12 | Constant in library | Remarks |
----------------- | ------ | ------------ | ------------------- |---------|
A0  | S01 | ADC (2)| | seems to be decoupled with 220k ohm |
D16 | S02 | GPIO16 (4) | | id on silkscreen for S02 is wrong - it is D16 instead of D10 |
D14 | S02, S03 | GPIO14 (5) | | |
D13 | S03, S04 | GPIO13 (7) | | | 
D12 | S04, S05 | GPIO12 (6) | | id on silkscreen for S05 is wrong - it is D12 instead of D04 |
D10 | S06 | GPIO10 (12) | | |
D09 | S06 | GPIO9 (11) | | |
D05 | S05, S08 | GPIO5 (20) | | |
D04 | S08 | GPIO4 (19) | | |
D02 | S09 | GPIO2 (17) | | |
D00 | S09 | GPIO0 (18) | | |
RXD | S07 | RXD0 (21) | | |
TXD | S07 | TXD0 (22) | | |
SDA | S10 | GPIO4 (19) | | |
SCL | S10 | GPIO5 (20) | | |
SDA | S11 | GPIO4 (19) | | |
SCL | S11 | GPIO5 (20) | | |

## Errors found so far
- all grove sockets (S01-S11) mounted the wrong way or used a wrong socket
- A0 (ADN) on socket pin 3 instead 4
- Silk screen label on socket S02 is wrong - correct is: GND, VCC, D14, D16
- Silk screen label on socket S05 is wrong - correct is: GND, VCC, D5, D12

## Links

[1]:http://www.smartarduino.com/view.php?id=95040
[2]:https://www.aliexpress.com/item/Original-DOIT-ESP8266-WiFi-Grove-Board-Kit-PMS5003-WiFi-Sensor-Remote-Control-Shield/32680953667.html
[3]:https://github.com/SmartArduino/