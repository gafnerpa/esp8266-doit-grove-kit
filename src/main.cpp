#include <Arduino.h>

// Connector layout
//
//  A0  NC     RXD TXD
// D14 D10     D04 D05
// D13 D14     D00 D02
// D12 D13     SDA SCL
// D05 D04     SDA SCL
// D09 D10


constexpr int8_t RELAIS_PIN = D10;

void setup() {
    pinMode(BUILTIN_LED, OUTPUT);
    pinMode(D10, OUTPUT);
}

void loop() {
    digitalWrite(BUILTIN_LED, LOW);
    digitalWrite(RELAIS_PIN, LOW);
    delay(500);
    digitalWrite(BUILTIN_LED, HIGH);
    digitalWrite(RELAIS_PIN, HIGH);
    delay(500);
}